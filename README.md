Programmed in Java, the game is played through the console window. 
The user guides a player through the DCU campus with a series of text-based commands. 
Each scenario has a different outcome and makes use of various methods and classes within the Java programming language. 
This type of project made up 10% of my grade and was the only one of its kind compared to the others which were presented for the assignment. 