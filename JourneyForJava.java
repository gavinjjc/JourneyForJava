
/** <h1>A simple text adventure based in DCU.</h1>
  * Players are required to choose from a number of scanarios which result in various different outcomes.
  *
  * @author Gavin Connorton
  * @author Conor McGrath
  * @author Scott Dermody
  * @author Alannah McCabe
  *
  * @version 4.0
  *
  * @since 2016-03-12
  *
  */

import java.util.Scanner;
class JourneyForJava
  {
  public static void main(String [] args)
  {
    Scanner in = new Scanner(System.in);

    // Creates player name based on user input
    System.out.println("Enter a name for your character...");
    String playerName = in.nextLine();

    // Asks the player to choose a difficulty setting
    System.out.println("Choose your difficulty... \n 1: Normal \n 2: Hard \n");
    //int difficulty = in.nextInt();
    String difficulty = in.nextLine();

    // Default Variables
    int defaultHealth = 50;
    int winCount = 0; // Variable to determine if winning message is shown once the game ends

    // Game Player
    Character player = new Character(playerName, 100);

    // Game Characters
    Character librarian = new Character("Brenda", defaultHealth);
    Character nerd = new Character("Evan", defaultHealth);
    Character weightlifter = new Character("The Weightlifter", defaultHealth);
    Character girlfriend = new Character("Girlfriend", 100); // Kiss adds health before final scenario
    Character starbucksLady = new Character("The Starbucks Lady", defaultHealth);
    Character ex = new Character("Sarah", defaultHealth);
    Character cyclist = new Character("The Cyclist", defaultHealth);
    Character bestfriend = new Character("Bestfriend", 100); // highfive adds heal
    Character sparman = new Character("Spar Man", defaultHealth);
    Character student = new Character("Student", defaultHealth);
    Character robber = new Character("The Robber", defaultHealth);
    Character roomie = new Character("Roomie", defaultHealth);
    Character cup = new Character("Cup", defaultHealth);

    // Game boolean
    boolean running = true;

    // Scenario 1
    System.out.println("\nYou grow tired of studying in the library and wish to get a coffee. \n What do you do? \n 1: Leave the library \n 2: Stay in your seat \n");

    // Boolean ensures game continues running thorugh scenarios
    // Break statements end the game once the player finishes the game or chooses the wrong scenario
    while(running)
    {
      String input = in.nextLine();

      if (input.equals("1"))
      {
        // Scenario 2
        System.out.println();
        System.out.println("Brenda the Librarian stops you in your tracks. You forgot to pay your fines. Now Brenda wants to kill you. \n What do you do? \n 1: Hit Brenda with your overdue book\n 2: Run for your life");

        if (difficulty.equals("2"))
        {
          System.out.println(" 3: Attempt to sneak past Brenda\n");
        }

        String librarianInput = in.nextLine();

        if (librarianInput.equals("1"))
        {
          player.attack(librarian);

          if (librarian.getHealth() < 1)
          {
            System.out.println("You have incapacitated Brenda!\n");
          }
        }
        else if (librarianInput.equals("2"))
        {
          System.out.println("Brenda locks the library door. You are now trapped. Your coffee money now has to go towards paying your fines! \n");
          librarian.attack(player);
          break;
        }
        else if (difficulty.equals("2") && librarianInput.equals("3"))
        {
          System.out.println("Brenda saw you before you exited the library and threw your overdue book at you causing damage to your health!");
          librarian.attack(player);
          break;
        }
      }
      else if (input.equals("2"))
      {
        System.out.println("You collapse from exhaustion due to a lack of caffeine!\n");
        break;
      }

      // User is given the option to travel to multiple locations
      System.out.println("Do you wish to go to \n 1: NuBar \n 2: Canteen \n 3: Helix \n 4: Business Cafe\n");
      String locationInput = in.nextLine();

      // Option 1
      if (locationInput.equals("1"))
      {
        // Option 1, Scenario 1
        System.out.println("You pass the pitches. \n What do you do? \n 1: Stop and watch \n 2: Keep walking past");
        if (difficulty.equals("2"))
        {
          System.out.println(" 3: Kick a ball back and play football\n");
        }

        String playerInput = in.nextLine();

        if (playerInput.equals("1"))
        {
            System.out.println("You enjoy the game and then continue to spar\n");
        }
        else if (playerInput.equals("2"))
        {
          System.out.println("You see someone miss a goal and laugh. He hears you and chases you. He hits you. You get up and run for your life!\n");
        }
        else if (difficulty.equals("2") && playerInput.equals("3"))
        {
          System.out.println("You pull your hamstring and need some time to stretch out!\n");
          break;
        }

        // Option 1, Scenario 2
        System.out.println("You've made it to Spar and the coffee is wthin reach. You ask Spar man for coffee but they have run out \n What do you do? \n 1: Take it in your stride and decide to go to NuBar \n 2: Throw a tantrum");
        if (difficulty.equals("2"))
        {
          System.out.println(" 3: Grab the last customer's coffee as it is the only one left\n");
        }

        String sparmanInput = in.nextLine(); // Player option

        if (sparmanInput.equals("1"))
        {
          System.out.println("You leave peacefully and head for NuBar \n");
        }
        else if (sparmanInput.equals("2"))
        {
          System.out.println("The security gaurd roughs you up a bit, you've learnt your lesson.!\n");
          sparman.attack(player);
          break;
        }
        else if (difficulty.equals("2") && sparmanInput.equals("3"))
        {
          System.out.println("You lunge at him for the coffee and end up spilling it on yourself. You have third degree burns!\n");
          sparman.attack(player);
          break;
        }

        // Option 1, Scenario 3
        System.out.println("You've reached the Nubar, your final destintion. You see the coffee stand, however there is a huge queue. \n What do you do? \n 1: Barge past everyone in the queue \n 2: Wait patiently in line knowing that your coffee is within touching distance while running the risk of fainting");
        if (difficulty.equals("2"))
        {
          System.out.println(" 3: Flirt with the server in the hope she will get you a coffee faster and for free\n");
        }

        String studentInput = in.nextLine();

        if (studentInput.equals("1"))
        {
          player.attack(student);

          if (student.getHealth() < 1)
          {
            System.out.println("You barge past the que and order before and angry mob of students complain behind you and want to kill you, but you got your coffee so you are happy!\n");
          }
          winCount++;
          break;
        }
        else if (studentInput.equals("2"))
        {
          System.out.println("You wait in line, however the wait is too long for you and you faint from exhaustion! You did not get you coffee in time! Hard luck \n");
          break;
        }
        else if (difficulty.equals("2") && studentInput.equals("3"))
        {
          System.out.println("You flirt with the waitress in the hope of a free coffee. However her boyfriend is standing behind you and attacks you. You fall to the ground and everyone laughs!\n");
          break;
        }
      }

      //  Option 2
      else if (locationInput.equals("2"))
      {
        // Option 2, Scenario 1
        System.out.println("You pass the science building. A nerd with a syringe jumps out of the bushes! Its Evan from primary school! You bullied him for six years straight and now he wants his revenge! \n What do you do? \n 1: Take the syringe from Evan and give him an Atomic Wedgie \n 2: Try to outrun Evan");
        if (difficulty.equals("2"))
        {
          System.out.println(" 3: Try to fight Evan\n");
        }

        String nerdInput = in.nextLine();

        if (nerdInput.equals("1"))
        {
          player.attack(nerd);

          if (nerd.getHealth() < 1)
          {
            System.out.println("Evan is left screaming in pain!\n");
          }
        }
        else if (nerdInput.equals("2"))
        {
          System.out.println("Evan caught up and atacked you with the syringe!\n");
          nerd.attack(player);
          break;
        }
        else if (difficulty.equals("2") && nerdInput.equals("3"))
        {
          System.out.println("Evan has been training for this day. He assaulted you right before he slipped and hurt his spleen!\n");
          nerd.attack(player);
          break;
        }

        // Option 2, Scenario 2
        System.out.println("You decide to take the longer way through the gym. You turn the corner and bump into someone from DCU Weightlifting. Now they're angry. \n You have multiple options \n 1: Distract the Weightlifter by saying 'Oooh look! A protein shake!' \n 2: Panic and run");
        if (difficulty.equals("2"))
        {
          System.out.println(" 3: Fight the Weightlifter\n");
        }

        String weightlifterInput = in.nextLine();

        if (weightlifterInput.equals("1"))
        {
          player.attack(weightlifter);

          if (weightlifter.getHealth() < 1)
          {
            System.out.println("The weightlifter opened the out of date shake and collapsed from suffocation! \n");
          }
        }
        else if (weightlifterInput.equals("2"))
        {
          System.out.println("The Weightlifter threw a barbell at you!\n");
          weightlifter.attack(player);
          break;
        }
        else if (difficulty.equals("2") && weightlifterInput.equals("3"))
        {
          System.out.println("The weightlifter is stronger than you and seriously injures you!\n");
          weightlifter.attack(player);
          break;
        }

        // Option 2, Bonus Scenario
        System.out.println("Just as your panic is over, your girlfriend finds you. She wants a kiss! \n What do you do? \n 1: Kiss your girlfriend. \n 2: Hug her instead\n");

        String girlfriendInput = in.nextLine();

        if (girlfriendInput.equals("1"))
        {
          System.out.println("You have kissed your girlfriend and your health is greatly increased!\n");
          girlfriend.kiss(player);
        }
        else if (girlfriendInput.equals("2"))
        {
          System.out.println("You have made your girlfriend feel awkward so you move on...\n");
        }

        // Option 2, Scenario 3
        System.out.println("You finally make it to the Canteen, however it seems to be closed. You spot the Starbucks Lady who is about to lock the last remaining door. \n You have two options \n 1: Perform a 'Rear Naked Choke Hold' on the Starbucks Lady and acquire the key. \n 2: Give up and collapse from exhaustion due to a lack of caffeine");

        String starbucksLadyInput = in.nextLine();

        if (starbucksLadyInput.equals("1"))
        {
          player.attack(starbucksLady);

          if (starbucksLady.getHealth() < 1)
          {
            System.out.println("You acquire the key to the Canteen (as well as a criminal record). You can now make the coffee which you've worked so hard for! \n");
            winCount++;
            break;
          }
        }
        else if (starbucksLadyInput.equals("2"))
        {
          System.out.println("You collapse from exhaustion due to a lack of caffeine!\n");
          starbucksLady.attack(player);
          break;
        }
      }


      // Option 3
      else if (locationInput.equals("3"))
      {
        // Option 3, Scenario 1
        System.out.println("You decide to take a shortcut through the Science building and see your ex-girlfriend Sarah walking towards you \n What do you do? \n 1: Confront her and tell her how much you hate her \n 2: Put your head down and keep walking by");
        if (difficulty.equals("2"))
        {
          System.out.println(" 3: Don't make eye contact and walk by fast hoping she doesn't notice you.\n");
        }

        String exInput = in.nextLine();

        if (exInput.equals("1"))
        {
          System.out.println("Sarah runs away in floods of tears!\n");
        }
        else if (exInput.equals("2"))
        {
          System.out.println("Sarah calls your name and runs after you asking to get back together which emotionally damages you and takes 25 health!\n");
          ex.HurtFeelings(player);
        }
        else if (difficulty.equals("2") && exInput.equals("3"))
        {
          System.out.println("Sarah reminds you of the reason you broke up after she runs after you and suprise attacks you with a rally of punches\n");
          ex.attack(player);
          break;
        }

        // Option 3, Scenario 2
        System.out.println("You decide to walk down by the bike racks. As you are walking a cyclist crashes into you and you both fall. \n Do you \n 1: Apologise. \n 2: Run away.");
        if (difficulty.equals("2"))
        {
          System.out.println(" 3: Attack the cyclist.\n");
        }

        String cyclistInput = in.nextLine();

        if (cyclistInput.equals("1"))
        {
          System.out.println("You apologised to the cyclist but they insisted it was not your fault but rather just unfortunate timing. \n");
        }
        else if (cyclistInput.equals("2"))
        {
          System.out.println("The Cyclist started cursing and shouting at you as you ran away! \n This deals 25 damage. \n");
          cyclist.HurtFeelings(player);
        }
        else if (difficulty.equals("2") && cyclistInput.equals("3"))
        {
          System.out.println("The Cyclist throws his bike at you!\n");
          cyclist.attack(player);
          break;
        }

        // Option 3, Bonus Scenario

        System.out.println("You've just gotten away and you see your best friend walking by. \n What do you do? \n 1: Give him a jumping high five. \n 2: Nod as you walk by.\n");

        String bestfriendInput = in.nextLine();

        if (bestfriendInput.equals("1"))
        {
          System.out.println("You give your friend a huge jumping high five which seriously increses your happiness and health.");
          bestfriend.highfive(player);
        }
        else if (bestfriendInput.equals("2"))
        {
          System.out.println("You acknowledge your friend as you walk by.");
        }

        // Option 3, Scanrio 3
        System.out.println("You eventually arrive at The Helix but there is a huge queue. \n You have some options \n 1: Skip the queue and hope that nobody says anything. \n 2: Give up and collapse from exhaustion due to a lack of caffeine.");
        if (difficulty.equals("2"))
        {
          System.out.println(" 3: Pull the fire alarm and make your own cup of coffee\n");
        }

        String queueInput = in.nextLine();

        if (queueInput.equals("1"))
        {
          System.out.println("You skip the queue and finally get to order the cup of coffee you craved so bad only to find out the machine is broken. \n You collapse from exhaustion due to a lack of caffeine! \n");
          winCount++;
          break;
        }
        else if (queueInput.equals("2"))
        {
          System.out.println("You collapse from exhaustion due to a lack of caffeine!\n");
          break;
        }
        else if (difficulty.equals("2") && queueInput.equals("3"))
        {
          System.out.println("Everyone exits the building and you finally get to make the coffee you wanted so bad even though you know you will have some serious explaining to do.\n");
          winCount++;
          break;
        }
      }

      // Option 4
      else if (locationInput.equals("4"))
      {
        // Option 4, Scenario 1
        System.out.println("You pass the bike stands. You see a man twice your size trying to steal a bike! \n What do you do? \n 1: Throw a brick at him \n 2: Call the police");
        if (difficulty.equals("2"))
        {
          System.out.println(" 3: Take matters into your own hands and fight the criminal\n");
        }

        String robberInput = in.nextLine();

        if (robberInput.equals("1"))
        {
          System.out.println("You are still high on adrenaline and throw a brick at the criminal! \n");
          player.attack(robber);
        }
        else if (robberInput.equals("2"))
        {
          System.out.println("The criminal saw you calling the police. He pushed you over, stands on your head and cycles away on the stolen bike!\n");
          robber.attack(player);
          break;
        }
        else if (difficulty.equals("2") && robberInput.equals("3"))
        {
          System.out.println("The criminal is much stronger than you and knocks you out with one punch!\n");
          robber.attack(player);
          break;
        }

        // Option 4, Scenario 2
        System.out.println("You walk into the business building. You see your roommate from last year who you havent spoken to since and you are wearing his jacket that you never gave back \n What do you do? \n 1: Pull the fire alarm to cause a distraction  \n 2: Panic and look down awkwardly");
        if (difficulty.equals("2"))
        {
          System.out.println(" 3: Trip up the person beside you before he can see you to distract him and run away\n");
        }

        String roomieInput = in.nextLine();

        if (roomieInput.equals("1"))
        {
          System.out.println("The distraction works, you can now walk past your old roommate");
        }
        else if (roomieInput.equals("2"))
        {
          System.out.println("Your old roommate spots you and confronts you about the jacket. He pushes you to the ground!\n");
          roomie.attack(player);
          break;
        }
        else if (difficulty.equals("2") && roomieInput.equals("3"))
        {
          System.out.println("He spots you and runs after you before he grabs you by his jacket. You fall to the ground!\n");
          roomie.attack(player);
          break;
        }

        // Option 4, Scenario 3
        System.out.println("You get to the Business Cafe at last! But you notice that they are completely out of coffee cups! \n What do you do? \n 1: Steal an old used cup from a table \n 2: Give up and take a seat");
         if (difficulty.equals("2"))
        {
          System.out.println(" 3: Steal the kettle and ingredients needed for the coffee and make it yourself\n");
        }

        String cupInput = in.nextLine();

        if (cupInput.equals("1"))
        {
          System.out.println("You get your caffeine fix!\n");
          winCount++;
          break;
        }
        else if (cupInput.equals("2"))
        {
          System.out.println("You fail to acquire any coffee and collapse on a seat in the Cafe!\n");
          break;
        }
        else if (difficulty.equals("2") && cupInput.equals("3"))
        {
          System.out.println("You pour the boiling water into your water bottle and get ready for your coffee. All of a sudden the plastic bottle melts in your hand! You collapse with the pain!\n");
          break;
        }
      }
    }

    if (winCount < 1)
    {
      System.out.println("\t You lost! Try again!\n"); // Print losing message
    }
    else if (winCount == 1)
    {
      System.out.println("\t Congratulations " + player.getName() + "! You have won! Thanks for playing!\n"); // Print winning message
    }
  }
}
