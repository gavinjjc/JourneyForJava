
/** <h1>A class file for Journey For Java, a simple text adventure based in DCU.</h1>
  * This file contains all of the main methods used in the game.
  *
  * @author Gavin Connorton
  * @author Scott Dermody
  *
  * @version 2.0
  *
  * @since 2016-03-12
  *
  */

class Character extends JourneyForJava
{
  // Variables
  public String name;
  public int health;

  // Constructor
  public Character (String nameString, int healthValue)
  {
    name = nameString;
    health = healthValue;
  }

  // Getter Methods
  public String getName()
  {
    return name;
  }
  public int getHealth()
  {
    return health;
  }

  // Setter Methods
  public void setHealth(int newHealth)
  {
    health = newHealth;
  }

  // Other Methods
  public void attack(Character other)
  {
    other.setHealth(0);
    System.out.println("\n" + this.getName() + " attacked " + other.getName() + "!");
    System.out.println(other.getName() + "'s Health: " + other.getHealth() + "\n");
  }

  public void HurtFeelings(Character other)
  {
    other.setHealth(other.getHealth() - 25);
    System.out.println("\n" + this.getName() + " emotionally hurt " + other.getName() + "!");
    System.out.println(other.getName() + "'s Health: " + other.getHealth() + "\n");
  }

  public void kiss(Character other)
  {
    other.setHealth(150);
    System.out.println(other.getName() + "'s Health: " + other.getHealth() + "\n");
  }

  public void highfive(Character other)
  {
    other.setHealth(150);
    System.out.println(other.getName() + "'s Health: " + other.getHealth() + "\n");
  }
}
